# Mongo Backup Container Image
### Description
This image is a tool thats helps making backups from mongo instances, encrypting and uploading to s3 buckets.
### Stack used
* [gnupg](https://www.gnupg.org/)
* [mongodb database tools](https://www.mongodb.com/try/download/database-tools)
* [aws cli](https://aws.amazon.com/cli/)
### Environment variables
* USERNAME
* PASSWORD
* HOST
* PORT (optional, default: 27017)
* GPG_KEY
* GPG_RECIPIENT
* S3_PATH (in the format "s3://bucket_name/dir/")
* S3_PROVIDER  (generic, default: aws)
* S3_ENDPOINT (required if S3_PROVIDER set to generic value)
* AWS_ACCESS_KEY_ID
* AWS_SECRET_ACCESS_KEY
* AWS_DEFAULT_REGION
### Restore from backup
In order to restore from backup first donwload from s3 bucket, decrypt the backup with required credentials and install mongodb database tools, run the following commands:
```sh
$ gpg --output <backup.tar.gz> --decrypt <backupfile.tar.gz.gpg>
$ mongorestore --uri="mongodb://<USERNAME>:<PASSWORD>@<HOST>:<PORT>" --authenticationDatabase=admin --gzip --archive=<backup.tar.gz>
```