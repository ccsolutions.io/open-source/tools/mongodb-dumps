#!/bin/bash
mkdir -p dump
if [[ -z "$USERNAME" ]]; then
        echo "Must provide USERNAME in environment."
        exit 2
fi
if [[ -z "$PASSWORD" ]]; then
        echo "Must provide PASSWORD in environment."
        exit 2
fi
if [[ -z "$HOST" ]]; then
        echo "Must provide HOST in environment."
        exit 2
fi
if [[ -z "$GPG_KEY" ]]; then
        echo "Must provide GPG_KEY in environment."
        exit 2
fi
if [[ -z "$GPG_RECIPIENT" ]]; then
        echo "Must provide GPG_RECIPIENT in environment."
        exit 2
fi
if [[ -z "$S3_PATH" ]]; then
        echo "Must provide S3_PATH in environment."
        exit 2
fi
if [[ -z "$AWS_ACCESS_KEY_ID" ]]; then
        echo "Must provide AWS_ACCESS_KEY_ID in environment."
        exit 2
fi
if [[ -z "$AWS_SECRET_ACCESS_KEY" ]]; then
        echo "Must provide AWS_SECRET_ACCESS_KEY in environment."
        exit 2
fi
if [[ -z "$PORT" ]]; then
        echo "No port provided, using default mongo port 27017 instead"
        PORT="27017"
fi
echo "$GPG_KEY" | gpg --batch --import
dbs=$(mongosh -u $USERNAME -p $PASSWORD --host $HOST --port $PORT --quiet --eval 'db.getMongo().getDBNames().forEach(function(db){print(db)})')
echo "$GPG_KEY" | gpg --batch --import
dbs=$(mongosh -u $USERNAME -p $PASSWORD --host $HOST --port $PORT --quiet --eval 'db.getMongo().getDBNames().forEach(function(db){print(db)})')
for db in $dbs; do
        if ! [ "$db" = "admin" ] && ! [ "$db" = "local" ] && ! [ "$db" = "config" ]; then
                backup_name="$(date +%Y%m%d%H%M%S)_$db"
                mkdir dump/$backup_name
                echo $backup_name
                mongodump --uri="mongodb://$USERNAME:$PASSWORD@$HOST:$PORT/$db" --authenticationDatabase=admin --gzip --out=dump/$backup_name
                tar -czvf $backup_name.tar.gz dump/$backup_name/
                encrypted_name="$backup_name.tar.gz.gpg"
                gpg --always-trust -r $GPG_RECIPIENT -o $encrypted_name --encrypt "$backup_name.tar.gz"
                case $S3_PROVIDER in
                generic)
                        aws s3 cp $encrypted_name $S3_PATH --endpoint-url "https://$S3_ENDPOINT"
                        ;;
                *)
                        aws s3 cp $encrypted_name $S3_PATH
                        ;;
                esac
        fi
done

